package main

import (
	"flag"
    "bufio"
    "io/ioutil"
    "strconv"
    "path/filepath"
    "fmt"
    "log"
    "os"
)

type PackageInfo struct {
	Name string
	Version string
	Description string
	IsExplicitInstalled bool
}

func getFlags(p PackageInfo) string {
	if p.IsExplicitInstalled {
		return "e"
	} else {
		return "d"
	}
}

func listFolders(path string) []string {
	var folders []string
    childs, err := ioutil.ReadDir(path)
    if err != nil {
        log.Fatal(err)
    }
 
    for _, c := range childs {
    	if c.IsDir() {
    		folders = append(folders, c.Name()) 
    	}
    }	
    return folders
}

func readLines(path string) ([]string, error) {
    file, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    var lines []string
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines, scanner.Err()
}

func joinToPacmanDescFile(path, name string) string{
	return filepath.Join(path, name, "/desc")
}

func getIndexOf(a []string, b string) int {
	for i, e := range a {
		if (e == b) {
			return i
		}
	}
	return -1
}

func getValueOf(lines []string, key string) (string,int) {
	indexKey := getIndexOf(lines, key)
	if indexKey == -1 || indexKey > len(lines) {
		return "", -1
	} else {
		return lines[indexKey+1], 0
	}
}


func parsePackageInfo(lines []string) PackageInfo{
	name, _ := getValueOf(lines, "%NAME%")
	version, _ := getValueOf(lines, "%VERSION%")
	description, _ :=  getValueOf(lines, "%DESC%")

	_, err := getValueOf(lines, "%REASON%")
	installedExplicid := (err == -1)

	return PackageInfo{
		Name: name,
		Version: version,
		Description: description,
		IsExplicitInstalled: installedExplicid,
	}
}

func queryPackageInfos(pacmanCachePath string) []PackageInfo {
	var packages []PackageInfo
    folders := listFolders(pacmanCachePath)
    for _, f := range folders {
    	file := joinToPacmanDescFile(pacmanCachePath, f)
    	lines, _ := readLines(file)
    	info := parsePackageInfo(lines)
    	packages = append(packages, info)
    }

    return packages	
}

func filterExplicitInstalled(packages []PackageInfo) []PackageInfo {
	var result []PackageInfo
	for _, p := range packages {
		if p.IsExplicitInstalled {
			result = append(result, p) 
		}
	}
	return result
}

func printTableSeperator() {
	for i := 0; i < 180; i++ {
		fmt.Fprint(os.Stdout, "=")
	}
	fmt.Fprint(os.Stdout, "\n")
}

func printTableRow(col1, col2, col3, col4 string) {
	pos_col2 := 20
	pos_col3 := 50
	pos_col4 := 80

	fmt.Fprint(os.Stdout, "\033[s")
	fmt.Fprint(os.Stdout, col1)
	fmt.Fprintf(os.Stdout, "\033[u\033[%vC%v", strconv.Itoa(pos_col2), col2)
	fmt.Fprintf(os.Stdout, "\033[u\033[%vC%v", strconv.Itoa(pos_col3), col3)
	fmt.Fprintf(os.Stdout, "\033[u\033[%vC%v", strconv.Itoa(pos_col4), col4)
	fmt.Fprintf(os.Stdout, "\n")
}

func printCount(cnt_all, cnt_explicit int) {
	fmt.Fprintf(os.Stdout, "Count: %v(%v)\n", strconv.Itoa(cnt_all), strconv.Itoa(cnt_explicit))
}

func main() {
	pacmanCache := flag.String("c", "/var/lib/pacman/local", "pacman cache folder")
	listExplicitOnly := flag.Bool("e", false, "list only explicit")
	flag.Parse()

	packages_all := queryPackageInfos(*pacmanCache)
	packages_explicit := filterExplicitInstalled(packages_all)

	printTableRow("Flags", "Name", "Version", "Description")
	printTableSeperator()

	if *listExplicitOnly {
	    for _, p := range packages_explicit {
    		printTableRow(getFlags(p), p.Name, p.Version, p.Description)
    	} 
	} else {
	    for _, p := range packages_all {
    		printTableRow(getFlags(p), p.Name, p.Version, p.Description)
    	}
	}

    printTableSeperator()
    cnt_all := len(packages_all)
    cnt_explicit := len(packages_explicit)
    printCount(cnt_all, cnt_explicit)

}